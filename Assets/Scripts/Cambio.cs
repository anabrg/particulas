using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cambio : MonoBehaviour
{
    public ParticleSystem brillos;

    public void CAzul()
    {
        var main = brillos.main;
        main.startColor = Color.blue;
    }

    public void CRojo()
    {
        var main = brillos.main;
        main.startColor = Color.red;
    }

    public void CVerde()
    {
        var main = brillos.main;
        main.startColor = Color.green;
    }

    public void Play()
    {
        // Modifica el m�dulo de emisi�n relacionado
        var emission = brillos.emission;
        emission.enabled = true;

    
    }
    public void Stop()
    {
        // Modifica el m�dulo de emisi�n relacionado
        var emission = brillos.emission;
        emission.enabled = false;


    }

}

